//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Bulb.h"
#include "uvpn_m.h"


using namespace std;

void
Bulb::handleMessage(cMessage *msg) {
	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);
	IceMessage inv = frame->getInvocation();

	cout << "  [" << getName() << "] " << "received invocation: " << inv.getIdentity()
		 << "." << inv.getMethod() << "(" << inv.getArg() << ")" << endl;

	if (string(inv.getIdentity()) == string(par("identity").stringValue())) {
		if (string(inv.getMethod()) == string("set")) {
			on_set(inv.getArg());
			send_load(inv.getArg());
		}
	}
	delete msg;
}

void
Bulb::on_set(bool state) {
	string icon;
	if (state) {
		icon = "block/bulbon";
	}
	else {
		icon = "block/bulboff";
	}

	cDisplayString& dispStr = getDisplayString();
	dispStr.setTagArg("i", 0, icon.c_str());
}

void
Bulb::send_load(bool state) {
	int load = par("connectedLoad");
	if (not state) {
		load = -load;
	}

	// Build invocation
	IceMessage inv;
	inv.setIdentity("loadMonitor");
	inv.setMethod("load");
	inv.setArg(load);

	// Build endpoint
	XBowEndpointInfo endp;
	endp.setMac(":vn1");

	// Build message label
	stringstream label;
	label << endl << inv.getIdentity() << "."
		  << inv.getMethod() << "(" << inv.getArg() << ")";

	// Builde frame
	XBowFrame* frame = new XBowFrame(label.str().c_str());
	frame->setInvocation(inv);
	frame->setDst(endp);

	send(frame, "xbow$o");
}


