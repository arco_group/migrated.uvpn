//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Button.h"
#include "uvpn_m.h"


using namespace std;

void
Button::initialize() {
	schedule_on();
	schedule_off();
}

void
Button::handleMessage(cMessage *msg) {
	if (msg->isSelfMessage()) {
		send(msg, "xbow$o");
		return;
	}
	delete msg;
}

void
Button::schedule_on() {
	int when = par("startAt");

	if (when < 1) {
		return;
	}

	XBowFrame* frame = build_message(true);
	scheduleAt(simTime()+when, frame);
}

void
Button::schedule_off() {
	int when = par("stopAt");

	if (when < 1) {
		return;
	}

	XBowFrame* frame = build_message(false);
	scheduleAt(simTime()+when, frame);
}

XBowFrame*
Button::build_message(bool state) {
	// build invocation
	IceMessage invocation;
	invocation.setIdentity("bulb");
	invocation.setMethod("set");
	invocation.setArg(state);

	// build destination
	XBowEndpointInfo dst;
	dst.setMac(":n1");

	// build label
	stringstream label;
	label << endl << "[" << dst.getMac() << "]"
		  << invocation.getIdentity() << "." << invocation.getMethod()
		  << "(" << invocation.getArg() << ")";

	// build frame
	XBowFrame* frame = new XBowFrame(label.str().c_str());
	frame->setInvocation(invocation);
	frame->setDst(dst);

	return frame;
}

