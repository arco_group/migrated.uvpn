//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "LoadMonitor.h"
#include "uvpn_m.h"


using namespace std;

#define TH1 30
#define TH2 50


LoadMonitor::LoadMonitor() :
	load(0) {
}

void
LoadMonitor::handleMessage(cMessage *msg) {
	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);
	IceMessage inv = frame->getInvocation();

	cout << "  [" << getName() << "] " << "received invocation: " << inv.getIdentity()
		 << "." << inv.getMethod() << "(" << inv.getArg() << ")" << endl;

	if (string(inv.getIdentity()) == string(par("identity").stringValue())) {
		if (string(inv.getMethod()) == string("load")) {
			on_load(inv.getArg());
		}
	}
	delete msg;
}

void
LoadMonitor::on_load(int reported_load) {
	load += reported_load;

	if (load > TH2) {
		report_level(CRITICAL);
	}
	else if (load > TH1) {
		report_level(WARNING);
	}
	else {
		report_level(NORMAL);
	}
}

void
LoadMonitor::report_level(Severity level) {
	// Build invocation
	IceMessage inv;
	inv.setIdentity("topic");
	inv.setMethod("set");
	inv.setArg(level);

	// Build dst endpoint
	XBowEndpointInfo endp;
	endp.setMac(":vn2");

	// Build label
	stringstream label;
	label << endl << "[" << endp.getMac() << "]"
		  << inv.getIdentity() << "." << inv.getMethod()
		  << "(" << inv.getArg() << ")";

	// Build Frame
	XBowFrame* frame = new XBowFrame(label.str().c_str());
	frame->setInvocation(inv);
	frame->setDst(endp);

	send(frame, "lo");
}
