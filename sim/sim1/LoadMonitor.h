//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef LOADMONITOR_H_
#define LOADMONITOR_H_


#include <csimplemodule.h>
#include "uvpn_m.h"

class LoadMonitor: public cSimpleModule {
public:
	LoadMonitor();

protected:
	void handleMessage(cMessage* msg);

private:
	void report_level(Severity level);
	void on_load(int reported_load);

	int load;
};

Define_Module(LoadMonitor);

#endif /* LOADMONITOR_H_ */
