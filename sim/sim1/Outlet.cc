//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Outlet.h"
#include "uvpn_m.h"


using namespace std;

void
Outlet::initialize() {
	if (int(par("connectedLoad")) > 0) {
		schedule_connect_load();
		schedule_disconnect_load();
	}
}

void
Outlet::schedule_connect_load() {
	int when = par("startAt");

	if (when > 0) {
		int load = par("connectedLoad");
		XBowFrame* frame = build_load_message(load);
		scheduleAt(simTime() + when, frame);
	}
}

void
Outlet::schedule_disconnect_load() {
	int when = par("stopAt");

	if (when > 0) {
		int load = par("connectedLoad");
		XBowFrame* frame = build_load_message(-load);
		scheduleAt(simTime() + when, frame);
	}
}

XBowFrame*
Outlet::build_load_message(int arg) {
	// build invocation
	IceMessage inv;
	inv.setIdentity("loadMonitor");
	inv.setMethod("load");
	inv.setArg(arg);

	// build dst endpoint
	XBowEndpointInfo endp;
	endp.setMac(":vn1");

	// build label
	stringstream label;
	label << endl << "[" << endp.getMac() << "]"
	      << inv.getIdentity() << "." << inv.getMethod()
		  << "(" << inv.getArg() << ")";

	// build frame
	XBowFrame* frame = new XBowFrame(label.str().c_str());
	frame->setInvocation(inv);
	frame->setDst(endp);

	return frame;
}

void
Outlet::handleMessage(cMessage *msg) {
	if (msg->isSelfMessage()) {
		handle_event(msg);
		return;
	}

	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);
	IceMessage inv = frame->getInvocation();

	cout << "  [" << getName() << "] " << "received invocation: " << inv.getIdentity()
		 << "." << inv.getMethod() << "(" << inv.getArg() << ")" << endl;

	string oid = par("identity");
	if (string(inv.getIdentity()) == oid) {
		if (string(inv.getMethod()) == string("set")) {
			on_set(inv.getArg());
		}
	}

	delete msg;
}

void
Outlet::handle_event(cMessage *msg) {
	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);
	IceMessage inv = frame->getInvocation();

	if (inv.getArg() > 0) {
		ui_plug_socket();
	}
	else {
		ui_unplug_socket();
	}

	send(msg, "xbow$o");
}

void
Outlet::on_set(int level) {
	if ((int(par("connectedLoad"))) > 0) {
		return;
	}

	if (level == CRITICAL) {
		ui_disable();
	}
	else {
		ui_enable();
	}
}

void
Outlet::ui_disable() {
	cDisplayString& dispStr = getDisplayString();
	dispStr.setTagArg("b", 3, "#E21D1D");
}

void
Outlet::ui_enable() {
	cDisplayString& dispStr = getDisplayString();
	dispStr.setTagArg("b", 3, "#A8D324");
}

void
Outlet::ui_plug_socket() {
	cDisplayString& dispStr = getDisplayString();
	dispStr.setTagArg("i", 0, "block/plug");
}

void
Outlet::ui_unplug_socket() {
	cDisplayString& dispStr = getDisplayString();
	dispStr.setTagArg("i", 0, "block/socket");
}

