
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef OUTLET_H_
#define OUTLET_H_

#include <csimplemodule.h>
#include "uvpn_m.h"

class Outlet: public cSimpleModule {
protected:
	void initialize();
	void handleMessage(cMessage* msg);

private:
	void ui_enable();
	void ui_disable();
	void ui_plug_socket();
	void ui_unplug_socket();

	void schedule_connect_load();
	void schedule_disconnect_load();
	XBowFrame* build_load_message(int arg);
	void handle_event(cMessage* msg);

	void on_set(int level);
};

Define_Module(Outlet);

#endif /* OUTLET_H_ */
