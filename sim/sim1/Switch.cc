//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Switch.h"
#include "SwitchPort.h"


using namespace std;

void
Switch::initialize() {
	sw_ports = SwitchPort::get_ports_from_xml(this, par("objects"));
	update_vnodes();
}

void
Switch::handleMessage(cMessage* msg) {
	if (not send_to_virtual_node(msg)) {
		send_to_other_ports(msg);
	}

	delete msg;
}

bool
Switch::send_to_virtual_node(cMessage* msg) {
	XBowFrame frame = get_frame_from_message(msg);
	string mac = frame.getDst().getMac();

	TCPSwitchPort* port = get_vnode_port(mac);
	if (port == NULL) {
		return false;
	}

	cout << "  [" << getName() << "] " << "Known destination ("
		<< mac << ")" << "; send to "
		<< port->proxy.getEndp().getHost() << ":"
		<< port->proxy.getEndp().getPort() << endl;

	port->send_message(frame);
	return true;
}

void
Switch::send_to_other_ports(cMessage* msg) {
	XBowFrame frame = get_frame_from_message(msg);
	string mac = frame.getDst().getMac();

	cout << "  [" << getName() << "] " << "Unknown destination ("
		 << mac << ")" << "; send to other switch ports" << endl;

	SwitchPort* arrival_port = get_arrival_port(msg);
	for (vector<SwitchPort*>::iterator i = sw_ports.begin(); i != sw_ports.end(); i++) {
		if (not arrival_port->is_equal(*i) and (*i)->is_default) {
			cout << "  [" << getName() << "] " << "  - using port " << (*i)->number << endl;
			(*i)->send_message(frame);
		}
	}
}

TCPSwitchPort*
Switch::get_vnode_port(string dst) {
	for (vector<VNode>::iterator it = vnodes.begin(); it != vnodes.end(); it++) {
		if ((*it).mac != dst) {
			continue;
		}

		return dynamic_cast<TCPSwitchPort*>((*it).port);
	}

	return NULL;
}

SwitchPort*
Switch::get_arrival_port(cMessage* msg) {
	if (msg->arrivedOn("xbow$i")) {
		return sw_ports[0];
	}

	UVPNSend info = check_and_cast<UVPNSegment*>(msg)->getInfo();
	for (vector<SwitchPort*>::iterator it = sw_ports.begin(); it != sw_ports.end(); it++) {
		TCPSwitchPort* port = dynamic_cast<TCPSwitchPort*>(*it);
		if (port == NULL) {
			continue;
		}

		if (port->servant_id == string(info.getIdentity())) {
			return port;
		}
	}

	return NULL;
}

XBowFrame
Switch::get_frame_from_message(cMessage* msg) {
	if (msg->arrivedOn("tcp$i")) {
		UVPNSegment* segment = check_and_cast<UVPNSegment*>(msg);
		XBowFrame frame = segment->getInfo().getFrame();
		frame.setName(msg->getName());
		return frame;
	}

	return *check_and_cast<XBowFrame*>(msg);
}

void
Switch::update_vnodes() {
	cXMLElement* table  = par("objects");
	cXMLElementList xml_nodes = table->getChildrenByTagName("vnode");
	for (cXMLElementList::iterator tag=xml_nodes.begin(); tag!=xml_nodes.end(); tag++) {

		int number = atoi((*tag)->getFirstChildWithTag("port")->getNodeValue());
		for (vector<SwitchPort*>::iterator it = sw_ports.begin()+1; it != sw_ports.end(); it++) {

			if ((*it)->number != number) {
				continue;
			}

			VNode node;
			node.mac = (*tag)->getFirstChildWithTag("mac")->getNodeValue();
			node.port = (TCPSwitchPort*)*it;

			vnodes.push_back(node);
			break;
		}
	}

}
