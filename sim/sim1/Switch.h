//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SWITCH_H_
#define SWITCH_H_

#include <csimplemodule.h>
#include "SwitchPort.h"

class VNode {
public:
	std::string mac;
	SwitchPort* port;
};

class Switch: public cSimpleModule {
protected:
	void initialize();
	void handleMessage(cMessage* msg);

private:
	bool send_to_virtual_node(cMessage* msg);
	void send_to_other_ports(cMessage* msg);

	SwitchPort* get_arrival_port(cMessage* msg);
	XBowFrame get_frame_from_message(cMessage* msg);
	TCPSwitchPort* get_vnode_port(std::string mac);
	void update_vnodes();

	std::vector<SwitchPort*> sw_ports;
	std::vector<VNode> vnodes;
};

Define_Module(Switch);

#endif /* SWITCH_H_ */
