
#include "SwitchPort.h"


using namespace std;

vector<SwitchPort*>
SwitchPort::get_ports_from_xml(cSimpleModule* module, cXMLElement* xml) {
	vector<SwitchPort*> retval;

	XBowSwitchPort* xbow_port = new XBowSwitchPort(module, "xbow");
	retval.push_back(xbow_port);

	cXMLElementList xml_ports = xml->getChildrenByTagName("port");
	for (cXMLElementList::iterator it = xml_ports.begin(); it != xml_ports.end(); it++) {
		cXMLElement* xml_proxy = (*it)->getFirstChildWithTag("proxy");

		TCPEndpointInfo endp;
		endp.setHost(xml_proxy->getFirstChildWithTag("tcp-host")->getNodeValue());
		endp.setPort(atoi(xml_proxy->getFirstChildWithTag("tcp-port")->getNodeValue()));

		TCPProxyInfo proxy;
		proxy.setIdentity(xml_proxy->getFirstChildWithTag("identity")->getNodeValue());
		proxy.setEndp(endp);

		TCPSwitchPort* port = new TCPSwitchPort(module, "tcp");
		port->proxy = proxy;
		port->number = atoi((*it)->getAttribute("id"));
		port->servant_id = (*it)->getFirstChildWithTag("servant-id")->getNodeValue();

		cXMLElementList defaults = xml->getFirstChildWithTag("default")->getChildrenByTagName("port");
		for (cXMLElementList::iterator it = defaults.begin(); it != defaults.end(); it++) {
			if (atoi((*it)->getNodeValue()) == port->number) {
				port->is_default = true;
				break;
			}
		}

		retval.push_back(port);
	}

	return retval;
}

XBowSwitchPort::XBowSwitchPort(cSimpleModule* module, string iface) :
	module(module),
	iface(iface) {
	number = 0;
	is_default = true;
}

void
XBowSwitchPort::send_message(XBowFrame frame) {
	for (int i=0; i<module->gateSize(iface.c_str()); i++) {
		string gname = iface + "$o";
		module->send(frame.dup(), gname.c_str(), i);
	}
}

bool
XBowSwitchPort::is_equal(SwitchPort* b) {
	XBowSwitchPort* other = dynamic_cast<XBowSwitchPort*>(b);
	if (other == NULL) {
		return false;
	}

	return other->iface == iface;
}

TCPSwitchPort::TCPSwitchPort(cSimpleModule *module, std::string iface) :
	module(module),
	iface(iface) {
	number = -1;
	is_default = false;
}

void
TCPSwitchPort::send_message(XBowFrame frame) {
	UVPNSend info;
	info.setIdentity(proxy.getIdentity());
	info.setFrame(frame);

	UVPNSegment* segment = new UVPNSegment(frame.getName());
	segment->setDst(proxy.getEndp());
	segment->setInfo(info);

	module->send(segment, "tcp$o");
}

bool
TCPSwitchPort::is_equal(SwitchPort* b) {
	TCPSwitchPort* other = dynamic_cast<TCPSwitchPort*>(b);
	if (other == NULL) {
		return false;
	}

	TCPEndpointInfo tendp = proxy.getEndp();
	TCPEndpointInfo oendp = other->proxy.getEndp();

	return (other->iface == iface and
			string(other->proxy.getIdentity()) == string(proxy.getIdentity()) and
			string(tendp.getHost()) == string(oendp.getHost()) and
			tendp.getPort() == oendp.getPort());
}













