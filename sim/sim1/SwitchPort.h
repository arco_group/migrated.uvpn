
#ifndef SWITCHPORT_H_
#define SWITCHPORT_H_

#include "omnetpp.h"
#include "uvpn_m.h"


class SwitchPort {
public:
	virtual ~SwitchPort() {};
	static std::vector<SwitchPort*> get_ports_from_xml(cSimpleModule* module, cXMLElement* xml);
	virtual void send_message(XBowFrame frame) = 0;
	virtual bool is_equal(SwitchPort* b) = 0;

    int number;
    bool is_default;
};

class TCPSwitchPort : public SwitchPort {
public:
	TCPSwitchPort(cSimpleModule* module, std::string iface);
	void send_message(XBowFrame frame);
	bool is_equal(SwitchPort* b);

	TCPProxyInfo proxy;
	std::string servant_id;

private:
	cSimpleModule* module;
	std::string iface;
};

class XBowSwitchPort : public SwitchPort {
public:
    XBowSwitchPort(cSimpleModule *module, std::string iface);
    void send_message(XBowFrame frame);
    bool is_equal(SwitchPort *b);

private:
    cSimpleModule *module;
    std::string iface;
};

#endif /* SWITCHPORT_H_ */
