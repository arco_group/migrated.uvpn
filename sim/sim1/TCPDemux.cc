//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TCPDemux.h"
#include "uvpn_m.h"


using namespace std;

void
TCPDemux::handleMessage(cMessage *msg) {
	if (msg->arrivedOn("tcp$i")) {
		send_to_application(msg);
	}
	else {
		send_to_network(msg);
	}

	delete msg;
}

void
TCPDemux::send_to_application(cMessage* msg) {
	UVPNSegment* segment = check_and_cast<UVPNSegment*>(msg);
	XBowFrame frame = segment->getInfo().getFrame();

	string label = get_frame_label(&frame);
	frame.setName(label.c_str());

	int port = segment->getDst().getPort();
	send(frame.dup(), "port$o", port);
}

string
TCPDemux::get_frame_label(XBowFrame* frame) {
	IceMessage inv = frame->getInvocation();

	stringstream label;
	label << endl << "[" << frame->getDst().getMac() << "]"
		  << inv.getIdentity() << "." << inv.getMethod()
		  << "(" << inv.getArg() << ")";

	return label.str();
}

void
TCPDemux::send_to_network(cMessage* msg) {
	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);

	TCPEndpointInfo endp;
	endp.setHost(par("gw_host"));
	endp.setPort(0);

	// Build invocation info
	UVPNSend info;
	info.setIdentity(par("gw_oid"));
	info.setFrame(*frame);

	// Build label
	string label = msg->getName();

	// Build segment
	UVPNSegment* segment = new UVPNSegment(label.c_str());
	segment->setInfo(info);
	segment->setDst(endp);

	send(segment, "tcp$o");
}

