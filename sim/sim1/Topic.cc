//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Topic.h"
#include "uvpn_m.h"


using namespace std;

void
Topic::initialize() {
	cXMLElement* table = par("subscribers");
	cXMLElementList rows = table->getChildrenByTagName("proxy");

	for (cXMLElementList::iterator it=rows.begin(); it!=rows.end(); it++) {
		string endp_type = (*it)->getFirstChildWithTag("endpoint")->getAttribute("type");

		ProxyInfo* proxy;
		if (endp_type == "tcp") {
			proxy = get_tcp_proxy_from_xml(*it);
		}
		else {
			proxy = get_xbow_proxy_from_xml(*it);
		}

		subscribers.push_back(proxy);
	}
}

void
Topic::handleMessage(cMessage *msg) {
	XBowFrame* frame = check_and_cast<XBowFrame*>(msg);
	IceMessage inv = frame->getInvocation();

	for (vector<ProxyInfo*>::iterator it=subscribers.begin();  it!=subscribers.end(); it++) {
		dispatch_invocation(inv, *it);
	}

	delete msg;
}

void
Topic::dispatch_invocation(IceMessage inv, ProxyInfo* dst) {
	if (dynamic_cast<TCPProxyInfo*>(dst) != NULL) {
		dispatch_tcp_invocation(inv, (TCPProxyInfo*)dst);
		return;
	}

	dispatch_xbow_invocation(inv, (XBowProxyInfo*)dst);
}

void
Topic::dispatch_tcp_invocation(IceMessage inv, TCPProxyInfo* dst) {
	cout << "  [" << getName() << "] " << "subscriber: '" << dst->getIdentity() << " -o:tcp -h "
		 << dst->getEndp().getHost() << " -p " << dst->getEndp().getPort() << "'" << endl;

	// build invocation
	inv.setIdentity(dst->getIdentity());

	// build endpoint
	TCPEndpointInfo endp = dst->getEndp();

	// build label
	stringstream label;
	label << endl << "[" << endp.getHost() << "]"
		  << inv.getIdentity() << "." << inv.getMethod()
		  << "(" << inv.getArg() << ")";

	// build segment
	ICESegment* segment = new ICESegment(label.str().c_str());
	segment->setInvocation(inv);
	segment->setDst(endp);

	send(segment, "tcp$o");
}

void
Topic::dispatch_xbow_invocation(IceMessage inv, XBowProxyInfo* dst) {
	cout << "  [" << getName() << "] " << "subscriber: '" << dst->getIdentity() << " -o:xbow -h "
		 << dst->getEndp().getMac() << "'" << endl;

	// build invocation
	inv.setIdentity(dst->getIdentity());

	// build endpoint
	XBowEndpointInfo endp = dst->getEndp();

	// build label
	stringstream label;
	label << endl << "[" << endp.getMac() << "]"
		  << inv.getIdentity() << "." << inv.getMethod()
		  << "(" << inv.getArg() << ")";

	// build frame
	XBowFrame* frame = new XBowFrame(label.str().c_str());
	frame->setInvocation(inv);
	frame->setDst(endp);

	send(frame, "port$o");
}

ProxyInfo*
Topic::get_tcp_proxy_from_xml(cXMLElement* xml) {
	TCPProxyInfo* retval = new TCPProxyInfo();

	TCPEndpointInfo endp;
	endp.setHost(xml->getElementByPath("endpoint/host")->getNodeValue());
	endp.setPort(atoi(xml->getElementByPath("endpoint/port")->getNodeValue()));

	retval->setIdentity(xml->getFirstChildWithTag("identity")->getNodeValue());
	retval->setEndp(endp);

	return retval;
}

ProxyInfo*
Topic::get_xbow_proxy_from_xml(cXMLElement* xml) {
	XBowProxyInfo* retval = new XBowProxyInfo();

	XBowEndpointInfo endp;
	endp.setMac(xml->getElementByPath("endpoint/mac")->getNodeValue());

	retval->setIdentity(xml->getFirstChildWithTag("identity")->getNodeValue());
	retval->setEndp(endp);

	return retval;
}
