//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TOPIC_H_
#define TOPIC_H_

#include <omnetpp.h>
#include <csimplemodule.h>
#include "uvpn_m.h"


class Topic: public cSimpleModule {
protected:
	void initialize();
	void handleMessage(cMessage* msg);

private:
	void dispatch_invocation(IceMessage inv, ProxyInfo* dst);
	void dispatch_tcp_invocation(IceMessage inv, TCPProxyInfo* dst);
	void dispatch_xbow_invocation(IceMessage inv, XBowProxyInfo* dst);
	ProxyInfo* get_xbow_proxy_from_xml(cXMLElement* xml);
	ProxyInfo* get_tcp_proxy_from_xml(cXMLElement* xml);
	std::vector<ProxyInfo*> subscribers;
};

Define_Module(Topic);

#endif /* TOPIC_H_ */
