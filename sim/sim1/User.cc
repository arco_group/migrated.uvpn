//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "User.h"
#include "uvpn_m.h"


using namespace std;

void
User::handleMessage(cMessage *msg) {
	ICESegment* segment = check_and_cast<ICESegment*>(msg);
	IceMessage inv = segment->getInvocation();

	cout << "  [" << getName() << "] " << " received invocation: " << inv.getIdentity()
		 << "." << inv.getMethod() << "(" << inv.getArg() << ")" << endl;

	int level = inv.getArg();
	ui_update_state(level);
	ui_notify_level(level);

	delete msg;
}

void
User::ui_update_state(int level) {
	string text("set to: ");

	vector<string> levels(3);
	levels[NORMAL] = "NORMAL";
	levels[WARNING] = "WARNING";
	levels[CRITICAL] = "CRITICAL";

	text += levels[level];
	bubble(text.c_str());

	getDisplayString().setTagArg("t", 0, (string("state: ") + levels[level]).c_str());
}

void
User::ui_notify_level(int level) {
	cDisplayString& dispStr = getDisplayString();

	string color;
	switch(level) {
	case NORMAL:
		color = "#A8D324";
		break;
	case WARNING:
		color = "#FF9909";
		break;
	case CRITICAL:
		color = "#E21D1D";
		break;
	}

	dispStr.setTagArg("b", 3, color.c_str());
}

