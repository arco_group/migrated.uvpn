//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "WAN.h"
#include "uvpn_m.h"


using namespace std;

void
WAN::handleMessage(cMessage *msg) {
	TCPSegment* seg = check_and_cast<TCPSegment*>(msg);
	string dst = seg->getDst().getHost();

	int index = get_gate_index(dst);
	if (index < 0){
		ev << "[ERROR] unknown destination: '" << dst
		   << ", discarding..." << endl;
		return;
	}

	send(msg, "tcp$o", index);
}

int
WAN::get_gate_index(string addr) {
	for (int i=0; i<gateSize("tcp"); i++) {
		cGate* tcp = gate("tcp$o", i);
		cGate* peer = tcp->getNextGate();
		cModule* dest = peer->getOwnerModule();

		if (string(dest->getName()) == addr) {
			return i;
		}
	}

	return -1;
}

