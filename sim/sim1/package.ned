// -*- mode: c++; coding: utf-8 -*-

package uvpn_sim1;
import ned.IdealChannel;


channel Wireless extends IdealChannel
{
    @display("ls=,0");
}

channel Internal extends IdealChannel
{
    @display("ls=dodgerBlue,1");
}

simple Servant
{
    parameters:
        string identity;
        int startAt = default(0);
        int stopAt = default(0);
}

simple LoadMonitor extends Servant
{
    parameters:
        @display("i=block/control");
        @class(LoadMonitor);

    gates:
        inout port;
        output lo;
}

simple Topic extends Servant
{
    parameters:
        @display("i=block/dispatch");
        @class(Topic);
        xml subscribers;

    gates:
        inout port;
        input lo;
        inout tcp;
}

simple TCPDemux
{
    parameters:
        @display("i=block/cogwheel");
        string gw_host;

        string gw_oid;
    gates:
        inout port[];
        inout tcp;
}

module Server
{
    parameters:
        @display("i=device/server2;bgb=,,#EDECEB");

    gates:
        inout tcp[];

    submodules:
        loadMonitor: LoadMonitor {
            @display("p=55,38");
            identity = "loadMonitor";
        }
        topic: Topic {
            @display("p=179,38");
            identity = "topic";
            subscribers = xmldoc("subscribers.xml");
        }

        tcpDemux: TCPDemux {
            @display("p=105,118");
        }
    connections:
        loadMonitor.port <--> Internal <--> tcpDemux.port++;
        topic.port <--> Internal <--> tcpDemux.port++;
        tcpDemux.tcp <--> Internal <--> tcp++;
        loadMonitor.lo --> Internal --> topic.lo;
        topic.tcp <--> Internal <--> tcp++;
}

simple User extends Servant
{
    parameters:
        @display("i=device/terminal");
        @class(User);

    gates:
        inout tcp;
}

simple WAN
{
    parameters:
        @display("i=misc/cloud");

    gates:
        inout tcp[];
}

simple Outlet extends Servant
{
    parameters:
        @display("i=block/socket");
        @class(Outlet);
        int connectedLoad = default(0);

    gates:
        inout xbow;
}

simple Bulb extends Servant
{
    parameters:
        @display("i=block/bulboff");
        @class(Bulb);
        int connectedLoad = default(0);

    gates:
        inout xbow;
}

simple Button extends Servant
{
    parameters:
        @display("i=block/switchoff");
        @class(Button);

    gates:
        inout xbow;
}

simple Switch
{
    parameters:
        @display("i=abstract/accesspoint");
        xml objects;

    gates:
        inout xbow[];
        inout tcp;
}

network Network
{
    parameters:
        @display("bgb=1014,447,#EDECEB");

    submodules:
        node1: Bulb {
            @display("p=327,105;r=100,,grey75,1;i=block/bulboff;is=n;t=:n1");
            identity = "bulb";
            connectedLoad = 40;
        }
        node2: Outlet {
            @display("p=111,105;r=100,,grey75,1;b=35,50,rect,#A8D324,,1;i=block/socket;t=:n2");
            identity = "outlet1";
            startAt = 2;
            stopAt = 3;
            connectedLoad = 20;
        }
        node3: Outlet {
            @display("p=111,332;r=100,,grey75,1;b=35,50,rect,#A8D324,,1;i=block/socket;t=:n3");
            identity = "outlet2";
        }
        switch_A: Switch {
            @display("p=220,217;r=100,,grey75,1");
            objects = xmldoc("tables.xml", "/root/table[@id='switch_A']");
        }
        wan: WAN {
            @display("p=541,216;is=vl");
        }
        user: User {
            @display("is=n;b=66,66,oval,#A8D324,black,1;p=367,361;t=state: NORMAL");
            identity = "user";
        }
        server: Server {
            @display("p=628,360;t=:vn1 /  :vn2,r;b=55,55,oval,#EDECEB,,0");
            tcpDemux.gw_host = "root_switch";
            tcpDemux.gw_oid = "root_switch-2";
        }
        switch_B: Switch {
            @display("r=100,,grey75,1;p=759,217");
            objects = xmldoc("tables.xml", "/root/table[@id='switch_B']");
        }
        node4: Outlet {
            @display("p=895,110;b=35,50,rect,#A8D324,,1;r=100,,grey75,1;i=block/socket;t=:n4");
            identity = "outlet3";
        }
        node5: Button {
            @display("p=895,321;i=block/switchoff;is=n;r=100,,grey75,1;t=:n5");
            identity = "button";
            startAt = 1;
            stopAt = 4;
        }

        root_switch: Switch {
            @display("p=541,77");
            objects = xmldoc("tables.xml", "/root/table[@id='root_switch']");
        }
    connections:
        node1.xbow <--> Wireless <--> switch_A.xbow++;
        node2.xbow <--> Wireless <--> switch_A.xbow++;
        node3.xbow <--> Wireless <--> switch_A.xbow++;
        node4.xbow <--> Wireless <--> switch_B.xbow++;
        node5.xbow <--> Wireless <--> switch_B.xbow++;

        switch_A.tcp <--> wan.tcp++;
        switch_B.tcp <--> wan.tcp++;
        server.tcp++ <--> wan.tcp++;
        wan.tcp++ <--> root_switch.tcp;
        user.tcp <--> wan.tcp++;
        server.tcp++ <--> wan.tcp++;
}

