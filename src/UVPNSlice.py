#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import os
import Ice

_path = os.path.join(os.path.dirname(__file__), "uvpn.ice")
Ice.loadSlice("{} -I{}".format(_path, Ice.getSliceDir()))

import UVPN
