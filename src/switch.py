#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import os
import sys
import Ice

from UVPNSlice import UVPN


class SwitchI(UVPN.Switch):
    def send(self, addr, payload, current=None):
        pass

    def add(self, addr, transceiver, current=None):
        pass

    def remove(self, addr, current=None):
        pass

    def find(self, addr, current=None):
        pass


class SwitchService(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("SwitchAdapter")
        adapter.activate()

        servant = SwitchI()
        oid = ic.stringToIdentity("Switch")
        proxy = adapter.add(servant, oid)

        print "Switch ready @ '{}'".format(proxy)

        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    service = SwitchService()
    config = os.path.join(os.path.dirname(__file__), "switch.cfg")
    sys.exit(service.main(sys.argv, config))
