// -*- mode: c++; coding: utf-8; tab-width: 4 -*-

#include <Ice/BuiltinSequences.ice>

module UVPN {

    interface Transceiver {
		void send(Ice::ByteSeq addr, Ice::ByteSeq payload);
	};

	interface Switch extends Transceiver {
		void add(Ice::ByteSeq addr, Transceiver* prx);
		void remove(Ice::ByteSeq addr);
		Transceiver* find(Ice::ByteSeq addr);
	};
};
