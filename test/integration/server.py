#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import os
import sys
import Ice


class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("ServerAdapter")
        adapter.activate()

        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    s = Server()
    config = os.path.join(os.path.dirname(__file__), "server.cfg")
    sys.exit(s.main(sys.argv, config))
