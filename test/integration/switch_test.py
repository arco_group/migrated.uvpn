# -*- mode: python; coding: utf-8 -*-

import hamcrest
from prego import TestCase, Task


class TestSwitch(TestCase):
    def test_redirects_invocation(self):
        # launch switch
        switch = Task(desc="UVPN Switch", detach=True)
        switch.command("$basedir/src/switch.py")

        # launch server and verify that is called
        server = Task(desc="Server", detach=True)
        server_cmd = server.command("$testdir/server.py")
        server.assert_that(server_cmd.stdout.content,
                           hamcrest.contains_string("set to True"))

        # launch client
        client = Task(desc="Client")
        client.command("$testdir/client.py")



